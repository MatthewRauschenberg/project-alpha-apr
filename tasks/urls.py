from tasks.views import create_task, my_tasks
from django.urls import path


urlpatterns = [
    path("mine/", my_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
